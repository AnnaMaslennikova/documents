grammar Ini;										//define a grammar called Ini
WS : [ \t\r\n]+ -> skip ;  							// skip spaces, tabs, newlines, \r (Windows)
section: '[' SectionName ']';
SectionName: [a-zA-Zа-яА-Я]+						//describe section in []
			| '#' [0-9] ('.'[0-9])*	[А-Яа-я]+ ;		
assign: ID '=' Expr  ; 							// match an assignment statement like "LAT=Ўирота" или "KEYLNG=5"
ID: [a-zA-Zа-яА-Я_]+ [a-zA_Zа-яА-Я0-9_]* ;
Expr: [0-9]+																		//only digits
	|[a-zA-ZА-Яа-я]+																//only letters
	| ([A-Z]* [0-9]*) '_' ([A-Z]* [0-9]*) '_' ([0-9]* [A-Z])									//like T_M3_0 or T_M3_0Y
	| [A-Z]+ ('_' [0-9]+)+ '.dat' 													//for T_01_02_08.dat 
	| [0-9]+ '.' [0-9]+ '.'* [0-9]* ' ' [a-zA-ZА-Яа-я]+	'.'* ([(+][а-я]+[)])*		//1.2.9. Cредние декадные суммы осадков  за период 1971-2000 гг.(мм)
	| [0-9]+ '.' [0-9]+ '.'* [0-9]*	' ' [a-zA-ZА-Яа-я]+ ','+ [0-9]+ '-' [0-9]+ [a-zA-ZА-Яа-я]+		
																					//1.2.7.  лиматические нормы, 1971-2000. јтмосферное давление на уровне станции
	| [a-zA-ZА-Яа-я]+ '(' [A-Z]+ ',"' [а-я]+ '",' [0-9]+ ':' [0-9]+ ',"' [а-я]+ '"' ')'    
																					// Chart(L,"ћес¤ц",1:12,"мб")
	|([-!"#$%&'()* ,./:;<=>?@[\\\]_`{|}~]* [a-zA-ZА-Яа-я])+							//for ..\SMap\StnClim.ini
																					//and 	Map_Clim.ini
	;

